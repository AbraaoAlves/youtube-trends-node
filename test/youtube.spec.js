const chai = require('chai');
const YoutubeService = require('../services/youtube');

chai.should();

let service;
describe('Service', function() {
  beforeEach(() => {
    service = new YoutubeService();
  });

  it('should return fixed youtube trends structure', async () => {
    const result = service.getTrendingVideos();
    result.should.be.a('promise');

    const trends = await result;
    trends[0].should.not.be.a('promise');
  });
});
