import express from 'express';
import { YoutubeService } from '../services/youtube';
import * as config from '../config.json';

const router = express.Router();
const service = new YoutubeService();

/* GET home page. */
router.get('/', async (req, res) => {
  console.log('country', req.query.country);
  const trends = await service.getTrendingVideos(req.query.country);
  res.render('youtube/index', {
    title: config.title,
    videos: trends,
    countryList: config.countryList,
  });
});

router.get('/:videoId', async (req, res) => {
  res.render('youtube/player', {
    title: config.title
  });
});

module.exports = router;
