
/**@type {HTMLSelectElement} */
const countrySelect = document.getElementById('country-select');

countrySelect.value = new URLSearchParams(window.location.search).get('country');

if (!countrySelect.value) {
  location.href = location.href+'?country=US'; 
}

countrySelect.addEventListener('change',({target}) => 
  (location.href = updateQueryStringParameter(location.href, 'country', target.value))
);

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}
