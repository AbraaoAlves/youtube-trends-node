import Axios from 'axios';
import * as config from '../config.json';
import moment from "moment";
import numeral from 'numeral';

const axios = Axios.create({
  baseURL: config.youtubeApi.endpoint
});

export class YoutubeService {
  getTrendingVideos(countryCode) {
    var params = {
      part: 'snippet',
      chart: 'mostPopular',
      regionCode: countryCode || 'US', // should be replaced with country code from countryList
      maxResults: '24',
      key: config.youtubeApi.key
    };

    return axios.get('/', {params})
      .then((res) => res.data.items)
      .then((itens) => 
        Promise.all(itens.map((video) => 
          YoutubeService.getVideoDetails(video)))
      );
  }

  static getVideoDetails(video) {
    var params = {
      part: 'statistics',
      id: video.id,
      key: config.youtubeApi.key
    };

    return axios.get('/', {params}).then(function(res) {
      var result = res.data;

      video.viewCount = numeral(result['items'][0].statistics.viewCount).format('0a');
      video.likeCount = numeral(result['items'][0].statistics.likeCount).format('0a');

      video.title =  video.snippet.title,
      video.thumbnail =  video.snippet.thumbnails.high.url,
      video.publishedAt =  moment(video.snippet.publishedAt).fromNow()

      return video;
    });
  }
}
